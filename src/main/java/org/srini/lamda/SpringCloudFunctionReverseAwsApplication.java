package org.srini.lamda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.srini.lamda.functions.ReverseFunction;
import org.srini.lamda.functions.UpperCaseFunction;

import java.util.function.Function;

@SpringBootApplication
public class SpringCloudFunctionReverseAwsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudFunctionReverseAwsApplication.class, args);
	}


	@Bean
	public Function<String,String> reverse(){
		return s -> new ReverseFunction().apply(s);
	}

	@Bean
	public Function<String,String> upperCase(){
		return s -> new UpperCaseFunction().apply(s) ;
	}
}


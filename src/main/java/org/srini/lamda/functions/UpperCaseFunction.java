package org.srini.lamda.functions;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UpperCaseFunction implements Function<String,String> {
    /**
     * Applies this function to the given argument.
     *
     * @param s the function argument
     * @return the function result
     */
    @Override
    public String apply(String s) {
        return s.toUpperCase();
    }

    /**
     * Returns a composed function that first applies the {@code before}
     * function to its input, and then applies this function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param before the function to apply before this function is applied
     * @return a composed function that first applies the {@code before}
     * function and then applies this function
     * @throws NullPointerException if before is null
     * @see #andThen(Function)
     */
    @Override
    public <V> Function<V, String> compose(Function<? super V, ? extends String> before) {
        System.out.println("before uppercase "+ before.toString());
        return Function.super.compose(before);
    }

    /**
     * Returns a composed function that first applies this function to
     * its input, and then applies the {@code after} function to the result.
     * If evaluation of either function throws an exception, it is relayed to
     * the caller of the composed function.
     *
     * @param after the function to apply after this function is applied
     * @return a composed function that first applies this function and then
     * applies the {@code after} function
     * @throws NullPointerException if after is null
     * @see #compose(Function)
     */
    @Override
    public <V> Function<String, V> andThen(Function<? super String, ? extends V> after) {
        System.out.println("after uppercase "+ after.toString());
        Function<String,V> _after = s -> (V) s.concat(" modified ");
        return Function.super.andThen(_after);
    }
}

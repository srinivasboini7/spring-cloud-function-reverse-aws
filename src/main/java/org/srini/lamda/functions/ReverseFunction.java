package org.srini.lamda.functions;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class ReverseFunction implements Function<String, String> {


    /**
     * Applies this function to the given argument.
     *
     * @param s the function argument
     * @return the function result
     */
    @Override
    public String apply(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
